<?php 
include 'views/header.php';
?>
        <div id="Slider">
            <div class="Container">
                <div id="SliderBox">
                    <a href=""><img class="Slide" src="img/apps/app1.png" alt=""></a>
                    <a href=""><img class="Slide" src="img/apps/app2.png" alt=""></a>
                    <a href=""><img class="Slide" src="img/apps/app3.png" alt=""></a>
                    <a href=""><img class="Slide" src="img/apps/app4.png" alt=""></a>
                </div>
                <img id="Phone" src="img/phone.png" alt="">
            </div> 
        </div>
        
        
        <div id="KeyFeatures">
            <div class="Container">
                    <h1 class="BlueFonts"><span id="FeatureBolded">Key</span><span id="FeatureNotBolded"> features</span></h1>
                <div id="KeyFeaturesBox">
                    <div class="Feature">
                        <div class="Subtitle">
                            <img src="img/coffeeCup.png" alt="image does not exist"><h3>Frelance Designers</h3>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                    <div class="Feature">
                        <div class="Subtitle">
                            <img src="img/tablet.png" alt="image does not exist"><h3>Big Design Agencies</h3>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                    <div class="Feature">
                        <div class="Subtitle">
                            <img src="img/laptop.png" alt="image does not exist"><h3>Big Design Agencies</h3>                        
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    </div>
                </div>                
            </div>
            
        </div>
        <div id="MainContent">
            <div class="Container">
            <div id="MainContentBox">
                <?php
                    include "includes/dbconnect.php";
                    $select = $pdo->prepare("SELECT * FROM content");
                    $select->execute();
                    $contents = $select->fetchAll(PDO::FETCH_ASSOC);
                        foreach($contents as $content){
                            echo '<div class="Content">';                           
                            echo '<div class="MainContentSubtitle">';
                            echo '<img src="'.$content["ImageLocation"].'" alt="image does not exist" height="48px" width="48px"><h3  class="BlueFonts"><i>'.$content['Subtitle'].'</i></h3>';
                            echo '</div>';
                            echo '<p>'.$content['Text'].'</p>';
                            echo '</div>';
                        }                        
                ?>                
            </div>
                <button class="BlueFonts">DEVELOPMENT</button>
                <button class="BlueFonts">APPSTORE</button>
            </div>
        </div>
        <div id="ClientTestimonials">
            <div class="Container">
                <p><span id="Ct1Bolded">Client</span><span id="Ct1">testimonials:</span><span id="Ct2" class="BlueFonts">We convert your designs to high quality,all browser compatible and valid HTML/CSS.</span><span id="Ct3">John Smith,Designer</span></p>
            </div>
        </div>

 <?php include 'views/footer.php'?>
