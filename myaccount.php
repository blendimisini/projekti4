<?php 
    include 'views/header.php';
    include 'includes/myaccount.inc.php';
 ?>  
<div>
    <form id="InputForm" style="padding: 150px 0;" action="<?php $_SERVER['PHP_SELF'];?>" method="post">
        <div class="Container">
            <label>Firstname:</label><input type="text" value="<?php echo $firstname; ?>" name="firstname">
            <span class="ContactFormError"><?php echo $firstname_error;?></span>
            <label>Lastname:</label><input type="text" value="<?php echo $lastname;?>" name="lastname">
            <span class="ContactFormError"><?php echo $lastname_error;?></span>
            <label>Email:</label><input type="email" value="<?php echo $email;?>" name="email">
            <span class="ContactFormError"><?php echo $email_error;?></span>
            <span class="Success"><?php echo $success;?></span>
            <button name="update">Update</button>
        </div>       
    </form>
</div>
<?php include "views/footer.php";?>