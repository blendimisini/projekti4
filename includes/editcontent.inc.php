
<?php
    
    $extension_error = $subtitle_error = $content_error = $uploading_error = '';
    $subtitle = $content = '';
    $success = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        include "includes/dbconnect.php";
        
        $phpFileUploadError = array(
            0 => 'There is no error, the file uploaded with success',
            1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
            2 => 'The file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
            3 => 'The file was only partially uploaded',
            4 => 'No file was uploaded',
            5 => 'Missing a temporary folder',
            6 => 'Failed to write file to disk',
            7 => 'A PHP extension stopped the file upload'
        );
        function test_input($data){
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
        
        
        $extensions = array('jpg','jpeg','png','gif');
        $file_ext = explode('.',$_FILES['image']['name']);
        $file_ext = end($file_ext);
        if(empty($_POST['subtitle'])){
            $subtitle_error = 'Subtitle is required!';
        }else{
            $subtitle =test_input($_POST["subtitle"]);
            if(strlen($subtitle)<=5){
                $subtitle_error = "Subtitle must be more than 5 characters!";
            }
        }
        if(empty($_POST['content'])){
            $content_error = 'Content is empty!';
        }else{
            $content = test_input($_POST["content"]);
            if(strlen($content)<=20){
                $content_error = "Content must be more than 20 characters!";
            }
        }
        if($_FILES['image']['error']){
            $uploading_error = $phpFileUploadError[$_FILES['image']['error']];
        }
        if(!in_array($file_ext,$extensions)){
            $extension_error = 'File-s of this type are not allowed!';
        }
        


        if($subtitle_error == "" and $content_error == "" and $extension_error == "" and $uploading_error == ""){            
            $name = '';
            if(move_uploaded_file($_FILES['image']['tmp_name'],'uploaded-Images/'.$_FILES['image']['name'])){
                $name = 'uploaded-Images/'.$_FILES['image']['name'];
            }

            $insert=$pdo->prepare('INSERT INTO content(Subtitle, ImageLocation, Text) VALUES (?,?,?)');
            $insert->execute([$subtitle,$name,$content]);
            $success = "Content added successfuly!";
            $subtitle = $content = $name = '';
        } 
        
    }