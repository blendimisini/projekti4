<?php
    require_once("dbconnect.php");

    $select = $pdo->prepare("SELECT * FROM users where Username != :Username");
    $select->execute([
        ":Username" => $_SESSION['Username']
    ]);
    $users = $select->fetchAll(PDO::FETCH_ASSOC);
   