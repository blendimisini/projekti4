<?php
    $userid_error = $password_error = "";
    $userid = $password = "";
    $success="";

    function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }


    if($_SERVER["REQUEST_METHOD"] == "POST"){
        require 'dbconnect.php';
       
        if(empty($_POST["userid"])){
                $userid_error = "E-mail or username is required";
        }else{
            
            $userid = test_input($_POST["userid"]);
            
        }

        if(empty($_POST["password"])){
            $password_error = "Password is required!";
        }else{
           $password = test_input($_POST['password']);
        }

        $select = $pdo->prepare("SELECT * FROM users WHERE Username = :username OR Email = :email");
        $select->execute([":username"=>$userid,":email"=>$userid]);
        $pw = $select->fetch(PDO::FETCH_ASSOC);
        
        if($pw != false){           
            $val = password_verify($password,$pw['Password']);
            
            if($val == true){
                session_start();
                $_SESSION['Firstname']=$pw['Firstname'];
                $_SESSION['Lastname']=$pw['Lastname'];
                $_SESSION['Username']=$pw['Username'];
                $_SESSION['Email']=$pw['Email'];
                $_SESSION['IsAdmin']=$pw['IsAdmin'];
                $_SESSION['Password']=$pw['Password'];
                header("Location: index.php?login=success");

                $success=$_SESSION['Firstname']." : ".$_SESSION['Lastname']." : ".$_SESSION['Username']." : ".$_SESSION['Email'];
            }else{
                $password_error = "Incorrect password!";
            }
        }else{
            $userid_error = "Username or email does not exists!";
        }

    }