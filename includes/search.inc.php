<?php
    require 'dbconnect.php';

    if (isset($_POST['input'])) {
        $name = $_POST['input'];

        $select = $pdo->prepare("SELECT Subtitle FROM content where name=?");
        $select->execute([$name]);
        $contents = $select->fetchAll();
        
        if (!empty($name)) {
            foreach ($contents as $content) {
                if (strpos(strtolower($content['Subtitle']), strtolower($name)) !== false) {
                    echo $user['Subtitle'] . '<br>';
                }
            }
        }
    }