<?php
    $firstname_error = $lastname_error = $username_error = $email_error = "";
    $firstname = $_SESSION['Firstname'];
    $lastname = $_SESSION['Lastname'];
    $email = $_SESSION['Email'];;
    $success='';
    
    
    function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    if(isset($_POST['update'])){
        require "dbconnect.php";
        
        function exists($username,$rowname,$database){
            $value=false;
            $query = $database->prepare('SELECT * FROM users WHERE '.$rowname.'=:uid');
            $query->execute([':uid'=>$username]);
            if($query->fetch()!=false){
                $value=true;
            }
            return $value;        
        }

        $select = $pdo->prepare("SELECT * FROM users WHERE Username = :username OR Email = :email");
        $select->execute([":username" => $_SESSION['Username'],":email" => $_SESSION['Email']]);
        $pw = $select->fetch(PDO::FETCH_ASSOC);
      
        if(empty($_POST["firstname"])){
            $firstname_error = "Firstname is required";
        }else{
            $firstname = test_input($_POST["firstname"]);
            if(!preg_match("/^[a-zA-Z]*$/",$firstname)){
                $firstname_error = "Only letters are allowed";
            }
            

        }
        if(empty($_POST["lastname"])){
            $lastname_error = "Lastname is required";
        }else{
            $lastname = test_input($_POST["lastname"]);
            if(!preg_match("/^[a-zA-Z]*$/",$lastname)){
                $lastname_error = "Only letters are allowed!";
            }
        }
        
        
        if(empty($_POST["email"])){
            $email_error = "E-mail is required";
        }else{
            
            $email = test_input($_POST["email"]);
            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                $email_error = "invalid e-mail format";
            }
            if( $email != $_SESSION['Email'] && exists($_POST['email'],'Email',$pdo)){
                $email_error="Email is alredy taken!";
            }
        }    
        if($firstname_error == "" && $lastname_error == "" && $email_error == ""){
                $update = $pdo->prepare("UPDATE users  SET Firstname = :Firstname, Lastname = :Lastname, Email = :Email WHERE Username =:Username ");
                $update->execute([
                    ":Firstname" => $firstname,
                    ":Lastname" => $lastname,
                    ":Email" => $email,
                    ":Username" => $_SESSION['Username']           
                ]);
               
                $success="Your info has been successfuly updated!";
               
                $_SESSION['Firstname'] = $firstname;
                $_SESSION['Lastname'] = $lastname;
                $_SESSION['Email'] = $email;
        }               
    }
