<?php
    $firstname_error = $lastname_error = $email_error = $username_error = $gender_error = $birthdate_error = $password_error = "";
    $firstname = $lastname = $email = $username = $gender = $birthdate = $password = $confirmpassword = '';
    $success='';
    
    
    function test_input($data){
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    if($_SERVER["REQUEST_METHOD"] == "POST"){
            require 'dbconnect.php';
            function exists($username,$rowname,$database){
                $value=false;
                $query = $database->prepare('SELECT * FROM users WHERE '.$rowname.'=:uid');
                $query->execute([':uid'=>$username]);
                if($query->fetch()!=false){
                    $value=true;
                }
                return $value;        
            }


            if(empty($_POST["firstname"])){
                $firstname_error = "First name is required!";
            }else{
                $firstname =ucfirst(test_input($_POST["firstname"]));
                if(!preg_match("/^[a-zA-Z]*$/",$firstname)){
                    $firstname_error = "Only letters are allowed!";
                }
            }
            if(empty($_POST["lastname"])){
                $lastname_error = "Last name is required";
            }else{
                $lastname = ucfirst(test_input($_POST["lastname"]));
                if(!preg_match("/^[a-zA-Z]*$/",$lastname)){
                    $lastname_error = "Only letters are allowed!";
                }
            }

            if(empty($_POST["email"])){
                    $email_error = "E-mail is required!";
            }else{
                
                $email = test_input($_POST["email"]);
                if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                    $email_error = "invalid e-mail format!";
                }
                if(exists($_POST['email'],'Email',$pdo)){
                    $email_error="Email is alredy taken!";
                }
                
            }

            if(empty($_POST["username"])){
                $username_error = "Username is required!";
            }else{
                if(!preg_match("/^[a-zA-Z0-9]*$/",$username)){
                    $username_error = "Only letters and numbers are allowed!";
                }
                if(exists($_POST['username'],'Username',$pdo)){
                    $username_error="Username alredy exists!";
                }
                $username = test_input($_POST["username"]);
            }
            
            if(empty($_POST["gender"])){
                $gender_error = "Please select your gender!";
            }else{
                $gender = test_input($_POST["gender"]);
            }
            if(empty($_POST["birthdate"])){
                $birthdate_error = "Birthdate is required!";
            }else{
                $birthdate = test_input($_POST["birthdate"]);
            }
            if(empty($_POST["password"])){
                $password_error = "Password is required!";
            }else{
                if(strlen($_POST['password']) < 8){
                    $password_error = "Password must be at least 8 characters!";
                }
                $password = password_hash(test_input($_POST["password"]),PASSWORD_DEFAULT);  
            }
            
        
        
        
        if($firstname_error == "" and $lastname_error == "" and $email_error == "" and $username_error == "" and $gender_error == "" and $birthdate_error == "" and $password_error == ""){            
            $insert=$pdo->prepare('INSERT INTO users(Firstname, Lastname, Email, Username, Gender, Birthdate, Password) VALUES (?,?,?,?,?,?,?)');
            $insert->execute([$firstname,$lastname,$email,$username,$gender,$birthdate,$password]);
            $success = "You have been successfuly registred!";
            $firstname = $lastname = $email = $username = $gender = $birthdate = $password = $confirmpassword = '';

        }        
        
    }
//mm-dd-yy
//yy-mm-dd
    
?>