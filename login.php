<?php 
    include 'views/header.php';
    include 'includes/login.inc.php';
 ?>    
    
    <form id="InputForm" style="padding: 200px 0;" action="<?php $_SERVER['PHP_SELF'];?>" method="post">
        <div class="Container">
        <input type="text" name="userid" placeholder="Username or email" value="<?php echo $userid; ?>">
        <span class="ContactFormError" ><?php echo $userid_error;?></span>
        <input type="password" name="password" placeholder="Password">
        <span class="ContactFormError"><?php echo $password_error;?></span>
        <button name="login">Login</button>
            <span class="Success"><?php echo $success; ?></span>
       </div>
    </form>
    
<?php include 'views/footer.php'; ?>