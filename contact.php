<?php
 include 'views/header.php';
 include 'includes/form-processor.php';
?>
   <div id="ContactForm">
           <div class="Container">
           <form action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
                <fieldset>
                     <legend>Contact us</legend>
                </fieldset>
                <fieldset  id="ContactInputOne">
                     <input type="text" placeholder="Name" value="<?php echo $name; ?>" name="name">
                     <span class="ContactFormError"><?php echo $name_error; ?></span>
               </fieldset>
                <fieldset id="ContactInputTwo">
                     <input type="text" placeholder="E-mail" value="<?php echo $email; ?>" name="email">
                     <span class="ContactFormError"><?php echo $email_error; ?></span>
               </fieldset>
                <fieldset id="TextArea">
                     <textarea placeholder="Message"name="message"><?php echo $message; ?></textarea>
                     <span class="ContactFormError"><?php echo $message_error; ?></span>
               </fieldset>                
                <button name="submit" type="submit" data-submit="...Sending">Submit</button>
                <div class="Success"><?php echo $succes ?></div>
           </form>
           </div>
       </div>
<?php include 'views/footer.php'?>
