       <footer>
            <div class="Container">
                <div class="FooterContent">
                    <div class="FooterBox">
                        <div id="SearchSite">
                            <label id="Label" class="BlueFonts">Search Site</label>
                            <input type="text" id="search-content" placeholder="type and hit enter to search site">
                        </div>
                        <div id="HowToContact">
                            <h3>How  to  contact  us</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                        <div id="StayUpdated">
                            <h3>Stay updated</h3>
                            <div id="Social">
                                <a href=""><img src="img/social/blog.png" alt=""></a>
                                <a href="https://www.facebook.com/"><img src="img/social/facebook.png" alt=""></a>
                                <a href="https://www.twitter.com/"><img src="img/social/Twitter.png" alt=""></a>
                                <a href="https://www.google.com/"><img src="img/social/googleplus.png" alt=""></a>
                            </div>
                        </div>
                    </div>                   
                </div>
            </div>
        </footer>
        <div id="Info">
            <div class="Container">
                <div id="DateAndStuff">
                    <p>@2012-2018 appmaker all right reserved</p>
                </div>
                <div id="SecondaryNav">
                    <nav>
                        <ul>
                            <li><a href="">Home</a></li>
                            <li><a href="about.php">About</a></li>
                            <li><a href="">Testimonials</a></li>
                            <li><a href="">Faq</a></li>
                            <li><a href="">Examples</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="js/toggle.js"></script>
        
    </body>
</html>