
<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>AppMaker | Home</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/editfeatures.css">
        <link rel="stylesheet" href="css/inputform.css">
        <link rel="stylesheet" media="screen and (max-width: 900px)" href="css/tablet.css">
        <link rel="stylesheet" media="screen and (max-width: 660px)" href="css/mobile.css">
        
       
    </head>
    <body>
        <header>
            <div class="Container">
                <div id="Logo">
                    <a href="index.php"><img src="img/logo.png" alt=""></a>
                </div>
                <div id="Toggle">
                        <button id="Button"></button>
                </div>
                <div id="Login">
                    <?php
                        if(isset($_SESSION['Username'])){
                            echo '<a href="includes/logout.inc.php">Logout</a>';
                            echo '<a href="myaccount.php">Edit my Account</a>';
                            if($_SESSION['IsAdmin'] == '1'){
                                echo '<a href="editcontent.php">Edit content</a>';
                            }                   
                        }
                        else{
                            echo '<a href="login.php">Login</a>
                            <a href="signup.php">Sign-up</a>';
                        }
                    ?>
                </div>
                
                <nav id="NavBox">                    
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="">Testimonials</a></li>
                        <li><a href="">Faq</a></li>
                        <li><a href="">Examples</a></li>
                        <li><a href="contact.php">Contact</a></li>  
                </div>
                </nav>
                
            </div>
        </header>
        